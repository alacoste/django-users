from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index),
    path('logout', views.loggout),
    path('checklogin', views.logged_user),
    path('<str:key>', views.get_content),
]